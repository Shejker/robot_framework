# robot_framework

```
git clone git@gitlab.com:Shejker/robot_framework.git
cd robot_framework
```

Zbudowanie obrazu: (Pierwszy build trwa +/- 10minut)

`docker build -t home.pl/robot .`


Uruchomienie testów w przeglądarce Google Chrome:

```
docker run \
    -v <ścieka do lokalnego folderu 'reports'>:/opt/robotframework/reports:Z \
    -v <ścieka do lokalnego folderu 'tests'>:/opt/robotframework/tests:Z \
    -e BROWSER=chrome \
	home.pl/robot (alternatywnie IMAGE_ID)
```



Uruchomienie testów w przeglądarce Mozilla Firefox:

```
docker run \
    -v <ścieka do lokalnego folderu 'reports'>:/opt/robotframework/reports:Z \
    -v <ścieka do lokalnego folderu 'tests'>:/opt/robotframework/tests:Z \
    -e BROWSER=firefox \
	home.pl/robot (alternatywnie IMAGE_ID)
```

**W folderze /tests przygotowane są przykładowe testy, które pozwalają na weryfikację poprawności zbudowania obrazu Dockerowego.**


Źródła:
- https://hub.docker.com/r/ppodgorsek/robot-framework/
- https://hub.docker.com/r/robotframework/rfdocker
